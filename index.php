<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP SC 02</title>
</head>
<body>
    <h1>LOOPS</h1>
    <?= forLoop(); ?>


    <h1>ARRAY MANIPULATION</h1>
    <?= array_push($students, 'Debbie Rosales'); ?>
    <pre><?= print_r($students); ?></pre>
    <p><?= count($students); ?></p>
    <?php array_unshift($students, 'Angelica Lim'); ?>
	<pre><?php print_r($students); ?></pre>
    <?php array_shift($students); ?>
	<pre><?php print_r($students); ?></pre>



</body>
</html>